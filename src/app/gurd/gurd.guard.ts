import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router, CanLoad, UrlSegment, Route } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../auth/services/auth.service';
import { NgxIndexedDBService } from 'ngx-indexed-db';
import { GlobalService } from '../core/services/global.service'
import { ClientsocketService } from '../workspace/services/clientsocket.service';
import { LocalStorageService } from '../workspace/services/local-storage.service';

@Injectable({
  providedIn: 'root'
})
export class GurdGuard implements CanActivate, CanActivateChild, CanLoad {




  canLoad(route: Route, segments: UrlSegment[]): boolean | Promise<boolean> | Observable<boolean> {
     console.log('can load gurd')
    return this.checkLogin();
    //return this.loadMeasssages();
  }


  constructor(private Authservice: AuthService,
    private router: Router,
    private db: NgxIndexedDBService,
    private global: GlobalService,
    private socket: ClientsocketService,
    private storage: LocalStorageService) {

  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      console.log('can activate guard')
      return true;
  }



  canActivateChild(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.canActivate(next,state)
      // return this.checkLogin(state.url);
  }

  checkLogin() {

    return this.db.getAll('details')
      .then((data) => {
        if (data.length) {
          this.router.navigate([this.global.redirectUrl])
          return true
        }


        this.router.navigate(['/auth/login'])
        return false
      })
}
}
  


  // checkLogin(url: string) {
  //   this.db.getAll('details')
  //   .then((user: any) => {
  //     if (user.length) {
  //       var lastLoggedIn = { lastLoggin: 0 }
  //       for (var i = 0; i < user.length; i++) {
  //         if (user[i].lastLogin > lastLoggedIn.lastLoggin) {
  //           lastLoggedIn = user[i]
  //         }
  //       }

  //       lastLoggedIn.lastLoggin = new Date().getTime()
  //       this.db.update('details', lastLoggedIn)
  //       this.global.redirectUrl = url
  //       return true
  //     }
  //     else{
  //    // this.global.redirectUrl = url
  //     this.router.navigate(['/auth/login'])
  //     return false
  //     }

  //   })

  //   return true;
  // }


  // loadMeasssages() {
  //   return this.db.getAll('Messages')
  //     .then(messages => {
  //       if (messages) {
  //         console.log(messages)
  //         this.storage.updateLocalMessages(messages);
  //         return true

  //       }

  //       else {
  //         this.socket.allMessages()
  //         return true
  //       }
  //     })



  //   // .then((user: any) => {

  //   //   if (user.length) {
  //   //     var lastLoggedIn = { lastLoggin: 0 }
  //   //     for (var i = 0; i < user.length; i++) {
  //   //       if (user[i].lastLogin > lastLoggedIn.lastLoggin) {
  //   //         lastLoggedIn = user[i]
  //   //       }
  //   //     }

  //   //     lastLoggedIn.lastLoggin = new Date().getTime()
  //   //     this.db.update('details', lastLoggedIn)

  //   //     return true
  //   //   }

  //   //   this.global.redirectUrl = url
  //   //   this.router.navigate(['/auth/login'])
  //   //   return false
  //   // })


  


