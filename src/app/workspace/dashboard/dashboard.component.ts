import { Component, OnInit } from '@angular/core';
import { GlobalService } from 'src/app/core/services/global.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(private global:GlobalService) { }

  ngOnInit() {
  }


  slideIn(items: HTMLElement[], dashboard: HTMLElement) {
    var total = 40 * items.length
    if (!items[0].style.display || items[0].style.display == 'none') {
      dashboard.style.overflow = 'visible'

      for (var i = 0; i < items.length; i++) {
        items[i].style.left = -(total) + 'px'
        items[i].style.display = 'block'
        total = total - 40
      }
    } else {
      dashboard.style.overflow = 'hidden'
      for (var i = 0; i < items.length; i++) {
        items[i].style.left = (total) + 'px'
        items[i].style.display = 'none'
        total = total - 40
      }
    }
  }


      //  slideDown(dashboard:HTMLElement){
      //    console.log(dashboard.style.bottom)
      //    if(!dashboard.style.bottom || dashboard.style.bottom.valueOf()=='0vh'){
      //      return dashboard.style.bottom ='-40vh'
          
      //    }
         
      //    return dashboard.style.bottom ='0vh'
      //  }
}
