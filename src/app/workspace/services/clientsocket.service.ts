import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { GlobalService } from 'src/app/core/services/global.service'
import { from, Observable } from 'rxjs';
import * as io from 'socket.io-client';


@Injectable({
  providedIn: 'root'
})


export class ClientsocketService {
  // messages = this.socket.fromEvent<MessageList>('allMessages')
  // newmessage = this.socket.fromEvent<MessageList>('newMessage')
  // newRequest = this.socket.fromEvent<Request>('forwading to the receiver')
  // newResponse = this.socket.fromEvent<Request>('forwading to the sender')

 
 
  private url = 'http://localhost:4201'
  private socket;


  constructor(
   
    private global: GlobalService,
    //private sockett:Socket
    ) {
 this.socket = io(this.url)

    }


  public sendMessage(message: IMessage) {
    this.socket.emit('sendMessage', message);
  }

  getMessages() {
    let observable = new Observable<{ mesage:IMessage }>(observer => {
      this.socket.on('newMessage', (msg) => {
        observer.next(msg);
      });
      return () => { this.socket.disconnect(); }
    });

    return observable;
  }
    


  //sending the request to the server
  public requestToserver(request:Request) {
    this.socket.emit('friendRequest', request)
    

    console.log("client request is", request)
  }

  //response from ServerFromClientRequest
  // public requestResponseFromServer(){

  // }
  //response fromClient
  public requestToserverFromClient(request: Request) {
    this.socket.emit("reques-accepted", request)

    console.log("acceptance send", request)



  }

  //request from the server


  //listening from the server event()
  public getFriendRequest() {
    let observable = new Observable<{ request: Request }>(observer => {
      this.socket.on('forwading to the receiver', (data) => {
        observer.next(data);
      });
      return () => { this.socket.disconnect(); }
    });

    return observable;
  }
  public getFriendRequestAcceptance() {
    let observable = new Observable<{ request: Request }>(observer => {
      this.socket.on('forwading to the sender', (data) => {
        observer.next(data);
      });
      return () => { this.socket.disconnect(); }
    });

    return observable;
  }

  public authenticate() {
    this.socket.emit('authentication', this.global.token, (err, success) => {
      console.log(err, ' ', success)
    })
  }


  allMessages() {
    this.socket.emit('allMessages')
  }
}

export interface MessageList {
  id: string,
  messages: [IMessage]
}

export interface IMessage {
  from: string,
  to: string,
  message: string,
  time: number,
  viewed: boolean
}
export interface Request {
  to: string,
  from: string,
  accept: boolean,
  time: number
}