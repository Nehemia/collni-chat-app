import { TestBed } from '@angular/core/testing';

import { ClientsocketService } from './clientsocket.service';

describe('ClientsocketService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClientsocketService = TestBed.get(ClientsocketService);
    expect(service).toBeTruthy();
  });
});
