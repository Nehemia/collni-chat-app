import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WorkspaceModule } from './workspace.module';
import { WorkspaceComponent } from './workspace.component';
import { HomeComponent } from './home/home.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { GroupComponent } from './group/group.component';
import { ChatComponent } from './chat/chat.component';
import { DiscoveryComponent } from './discovery/discovery.component';


const routes: Routes = [
     {path: '',component :WorkspaceComponent, children: [
    { path: 'home', component: HomeComponent,data: { animation: 'isLeft'}},
    { path: 'dashboard', component: DashboardComponent },
    { path: 'discovery', component: DiscoveryComponent ,data:{animation :'isLeft'}},
    {path : 'groups' ,component :GroupComponent ,data: { animation: 'isRight' }},
    {path : 'group/:id' ,component :GroupComponent , data: { animation: 'group' }},
    {path :  'chat' ,component : ChatComponent},
    {path : '' , redirectTo :"/login" ,pathMatch : "full" }]}
]
;

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WorkspaceRoutingModule { }
