import { Component, OnInit, Pipe } from '@angular/core';
import { AuthService } from 'src/app/auth/services/auth.service';
import { pipe, observable, from } from 'rxjs';
import { NgForm } from '@angular/forms';
import { GlobalService } from 'src/app/core/services/global.service';
import { ClientsocketService, Request } from '../services/clientsocket.service';
import { request } from 'http';


@Component({
  selector: 'app-discovery',
  templateUrl: './discovery.component.html',
  styleUrls: ['./discovery.component.scss']
})




@Pipe({
  name: 'filter'

})
export class DiscoveryComponent implements OnInit {
  people = [];
  olly = ['sring', 'ging', 'ming']
  username: string;

  messageArray:Array<{request:Request}> = [];

  // private url = 'http://localhost:4201'

  constructor(private auth: AuthService,
    private global: GlobalService,
    private clientSocket: ClientsocketService,) {

    //  this.socket.getFriendRequest()
    //  .subscribe(data=> this.messageArray.push(data));

    //  this.socket.newRequest;
      
     }


     
  ngOnInit() { }

  //calling the discovery fuction in authservice
  public discovery(search: NgForm) {
    console.log("collin", search)
    this.auth.userDiscovery(search.value.username)
      .subscribe((observable) => {
        console.log("dicovery working", observable)
        //console.log("data is",data)

        this.people = observable.data.discovery
      
      });
  }
//sendin request to the client socket

  public SendRequest(receiver){
    console.log("receiver,",receiver)
    console.log("sender id,",this.username)

    var r={
      to:receiver,
      from:this.global.currentUser,
      accept:false,
      time:new Date().getTime()
    }

    console.log("request is",r);
    //this.socket.requestToserver(r)
    this.clientSocket.requestToserver(r)
    
  
  }

  // public replyRequest(sender:string){
  //   var response={
  //     to:sender,
  //     from:this.global.currentUser,
  //     accept:true,
  //     time:new Date().getTime()
  //   }

  //   console.log('response is,',response);
  //   this.socket.requestToserverFromClient(response);
  // }

}



