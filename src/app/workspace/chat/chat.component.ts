import { Component, OnInit } from '@angular/core';
import {WorkspaceService} from '../workspace.service';
import { NgForm } from '@angular/forms';
import { of, UnsubscriptionError } from 'rxjs';
import { Observable } from 'apollo-link';
import { ActivatedRoute, ParamMap } from '@angular/router';
import {ClientsocketService } from '../services/clientsocket.service'
import { GlobalService } from 'src/app/core/services/global.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {
        message:string;
        newMessage=[];
      ;
        

  constructor(private WorkspaceService :WorkspaceService,
              private route: ActivatedRoute ,
              private socket: ClientsocketService,
              private global : GlobalService) {
   
   }

   ngOnInit() {
    this.global.showdashboard = false
    // this.message$ = this.route.paramMap.pipe(
    //   switchMap((params : ParamMap) =>
    //     this.socket.getUserChats(params.get('id'))
    // )) 

  }

  public sendMessage(message: NgForm){
    //cannot send an empty unless whitespace explicily set
    if(message.valid){
      var to = this.route.snapshot.paramMap.get('id')
      var m = {
        to,
        from: '',
        message: message.value.message,
        time:new Date().getTime(),
        viewed: false
      }
      console.log("message is",m)
      this.socket.sendMessage(m)
    
  }


    // for(var i = 0; i < this.socket.texts.length; i++){
    //   if(this.socket.texts[i].id = to){
    //     this.socket.texts[i].messages.push(m)
    //   }
    // }

    message.reset();
  }

//   getMessages() {
//     let observable = new Observable(observer => {
//       this.socket.on('newMessage', (data) => {
//         observer.next(data); 
//         newMessage=observable.dataoo   
//       });
//       return () => {
//         this.socket.disconnect();
//       };  
//     })     
//     return observable;
//   }  
// }

  

}
