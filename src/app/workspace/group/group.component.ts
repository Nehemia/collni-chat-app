import { Component, OnInit } from '@angular/core';
import { NgxIndexedDBService } from 'ngx-indexed-db';
import { Observable } from 'rxjs';
import { ClientsocketService } from '../services/clientsocket.service';
import { LocalStorageService } from '../services/local-storage.service';
import { GlobalService } from 'src/app/core/services/global.service';

@Component({
  selector: 'app-group',
  templateUrl: './group.component.html',
  styleUrls: ['./group.component.scss']
})
export class GroupComponent implements OnInit {
  //getting messages from the server
  messages: Observable<any>;

  constructor(private db: NgxIndexedDBService ,
              private  socket:ClientsocketService ,
              private  storage : LocalStorageService,
              private global : GlobalService
               ) { }
 

  ngOnInit() {
    //global variable to show the dashboard
    this.global.showdashboard = false
    //check if indexdb has messages
    // if (!this.storage.messages.length) {
    //   this.socket.allMessages();
    //   this.socket.messages.subscribe(obs => {
    //     this.storage.messages.push(obs);
    //   }}}
  
  }
}

        
        


    
