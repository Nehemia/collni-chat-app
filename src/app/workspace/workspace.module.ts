import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';


import { WorkspaceRoutingModule } from './workspace-routing.module';
import { WorkspaceComponent } from './workspace.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HomeComponent } from './home/home.component';
import { GroupComponent } from './group/group.component';
import { ChatComponent } from './chat/chat.component';
import { WorkspaceService } from './workspace.service';
import {NgxIndexedDBModule , DBConfig} from 'ngx-indexed-db';
import { DiscoveryComponent } from './discovery/discovery.component';
import { ClientsocketService } from './services/clientsocket.service';




// const dbconfig : DBConfig ={
//   name : 'MyDb',
//   version : 1 ,
//   objectStoresMeta :[
//     {
//     store : 'messages',
//     storeConfig :{ keyPath : 'id', autoIncrement : true},
//     storeSchema : [
//       {name : 'to' ,keypath:'to',options:{unique:false}},
//       {name : 'from' ,keypath:'from',options:{unique:false}},
//       {name : 'message' ,keypath:'message',options:{unique:false}},
//       {name : 'user' ,keypath:'',options:{unique:false}}
    
//     ]
//     },
//     {
//       store : 'details',
//       storeConfig : {keyPath : 'id' ,autoIncrement : true},
//       storeSchema : [
//         {name : 'username' ,keypath:'username',options:{unique:false}},
//         {name : 'token' ,keypath:'token',options:{unique:false}},
//         {name : 'loggedIn' ,keypath:'loggedin',options:{unique:false}},
//         {name : 'lastlogged' ,keypath:'lastloddedIn',options:{unique:false}}
      
//       ]
//     }
//   ]
// }




@NgModule({
  declarations: [
    WorkspaceComponent ,
    DashboardComponent,
    HomeComponent,
    GroupComponent,
    ChatComponent,
    DiscoveryComponent

    
  ],
  imports: [
    CommonModule,
    FormsModule,
    WorkspaceRoutingModule,
   // NgxIndexedDBModule.forRoot(dbconfig),
    
  ],
 providers: []
})
export class WorkspaceModule { }
