import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';




import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { HomeComponent } from './workspace/home/home.component';
import { NotFoundComponent } from './core/not-found/not-found.component';

//the object routes to contain all the routing
const routes: Routes = [
 
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
