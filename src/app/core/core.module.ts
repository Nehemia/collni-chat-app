import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CoreRoutingModule } from './core-routing.module';
import { CoreComponent } from './core.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { WorkspaceModule } from '../workspace/workspace.module';
import { AuthModule } from '../auth/auth.module';
import { WorkspaceRoutingModule } from '../workspace/workspace-routing.module';


@NgModule({
  declarations: [
    CoreComponent,
    NotFoundComponent
  ],
  imports: [
    CommonModule,
    CoreRoutingModule,
    WorkspaceModule,
    AuthModule,
    WorkspaceRoutingModule
  ],
  exports :[
    CoreComponent      //we are exporting the component to others . want to start this one
  ]
})
export class CoreModule { }
