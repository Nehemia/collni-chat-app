import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from '../workspace/home/home.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { GurdGuard } from '../gurd/gurd.guard';


const routes: Routes = [
  {path : 'auth' ,loadChildren : 'src/app/auth/auth.module#AuthModule'},  // adding the  auth routing
  {
    path: 'user',loadChildren: 'src/app/workspace/workspace.module#WorkspaceModule'
  },
  //   path: 'user', canActivateChild:[GurdGuard],loadChildren: 'src/app/workspace/workspace.module#WorkspaceModule'
  // },
  // // {path : 'home' , component : HomeComponent },
  // {path : 'not-found' , component : NotFoundComponent },
  // {path : '**' , redirectTo :"/not-found " ,pathMatch : "full"}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CoreRoutingModule { }
