import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './workspace/home/home.component';
import { NotFoundComponent } from './core/not-found/not-found.component';
import { GraphQLModule } from './graphql.module';
import { HttpClientModule } from '@angular/common/http';
import { AuthModule } from './auth/auth.module';
import { CoreModule } from './core/core.module';
import { WorkspaceModule } from './workspace/workspace.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ClientsocketService } from './workspace/services/clientsocket.service';

import {NgxIndexedDBModule , DBConfig} from 'ngx-indexed-db';



const dbconfig : DBConfig ={
  name : 'MyDb',
  version : 1 ,
  objectStoresMeta :[
    {
    store : 'messages',
    storeConfig :{ keyPath : 'id', autoIncrement : true},
    storeSchema : [
      {name : 'to' ,keypath:'to',options:{unique:false}},
      {name : 'from' ,keypath:'from',options:{unique:false}},
      {name : 'message' ,keypath:'message',options:{unique:false}},
      {name : 'user' ,keypath:'',options:{unique:false}}
    
    ]
    },
    {
      store : 'details',
      storeConfig : {keyPath : 'id' ,autoIncrement : true},
      storeSchema : [
        {name : 'username' ,keypath:'username',options:{unique:false}},
        {name : 'token' ,keypath:'token',options:{unique:false}},
        {name : 'loggedIn' ,keypath:'loggedin',options:{unique:false}},
        {name : 'lastlogged' ,keypath:'lastloddedIn',options:{unique:false}}
      
      ]
    }
  ]
}



@NgModule({
  declarations: [
    AppComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    GraphQLModule,
    HttpClientModule,
    CoreModule,       //here we are importing that core module in order to access all the components in core
    WorkspaceModule,
    BrowserAnimationsModule,
    NgxIndexedDBModule.forRoot(dbconfig)

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
