import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Apollo } from 'apollo-angular';


import { registerLocaleData } from '@angular/common';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { WorkspaceService } from 'src/app/workspace/workspace.service';
import { NgxIndexedDBService} from 'ngx-indexed-db';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  constructor(private router : Router,
              private auth :AuthService ,
              private Workspace:WorkspaceService,
              private db:NgxIndexedDBService
              
              ) { }

  ngOnInit() {
  }
   

 public handlingForm(form:NgForm){
   console.log(form)
   this.auth.register(form.value.username ,form.value.Password)
   .subscribe(({data})=> {               //notification back
     console.log(data)
     this.Workspace.token = data

     this.db.add('details', { username: form.value.username, token:this.Workspace.token})
     this.router.navigate(['/user/home'])
     this.router.navigate(['/home'])    //router for navigating to another page
   }, error =>{
     console.log(error ,'error')

   })
 }
}
