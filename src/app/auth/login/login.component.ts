import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { WorkspaceService } from 'src/app/workspace/workspace.service';
import { NgxIndexedDBService } from 'ngx-indexed-db';
import { GlobalService } from 'src/app/core/services/global.service';

@Component({

  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})


export class LoginComponent implements OnInit {

  logInResults: string

  constructor(
    private auth: AuthService,
    private router: Router,
    private workstation: WorkspaceService,
    private db: NgxIndexedDBService,
    private global: GlobalService
  ) { }



  ngOnInit() {}


  public handlingForm(form: NgForm) {
    console.log(form)
    if (form.valid) {
      this.auth.Login(form.value.username, form.value.Password)
        .subscribe(({ data, loading }) => {
          console.log(data)
          this.auth.IsLoaggedin = true;
          this.workstation.token = data.login.token
  

          if(data.login.error){
            this.router.navigate(['/auth/login'])
          }
          else{
            this.db.clear('details')
            .then(response =>{
                       this.db.add('details', { username: form.value.username,token: this.workstation.token})
                        .then(res => {
                        if (this.global.redirectUrl)
                        this.router.navigate([this.global.redirectUrl])
                        else
                        this.router.navigate(['/user/home'])
                        })
                        .catch(err => {
                        //user exists in db, already logged in
                        if (err.target.error.code === 0) {
                        this.db.getByIndex('details', 'username', form.value.username)
                        .then(user => {
                        this.db.update('details', { id: user.id, 
                                                    loggedIn: 1, 
                                                    username: form.value.username, 
                                                    token: data.login, 
                                                    lastLogin: new Date().getTime() })

                        })
                        }
                        })
            })
          }
        })
    }

  }
}