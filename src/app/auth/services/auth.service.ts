import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';




@Injectable({
  providedIn: 'root'
})
export class AuthService {
          IsLoaggedin:boolean =true;
          redirectUrl:string; 



  constructor(private apollo: Apollo) { }




  //this allows to write graghql commands that ot understands
  private regesterMutation = gql`
   mutation Register($username : String , $password:String) 
   {
     register(username:$username , password:$password)
   }
   `
  //the loging in query 
   private loginQuery  = gql`
   query Login($username :String , $password : String){
    login(username :$username ,password :$password){
      token
      error
    }
   }
   `

   //the query for discovering users
   private discoverQuery = gql`
   query Discovery($username :String){
     discovery(username:$username){
       username
     }
   }
   `

   //register fuction
  public register(username: string, password: string) {
    return this.apollo.mutate<string>({
      mutation: this.regesterMutation,
      variables: {
        username,
        password
      }

    }
    
    );
   
  }



  


// the loging fuction
  public Login(username:string ,password :string){
    return this.apollo.watchQuery<any>({
       query : this.loginQuery ,
       variables :{
         username :username,
         password : password
       }
     }).valueChanges       //for it to return observables
  } 




  //the discovery option

  public userDiscovery(username:string){
    console.log('joseph')
    return this.apollo.watchQuery<any>({
      query:this.discoverQuery,
      variables :{
        username
      }
    }).valueChanges
  }
}