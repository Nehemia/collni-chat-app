import {
    trigger,
    transition,
    style,
    query,
    group,
    animate
} from '@angular/animations'

export const fader = trigger('fadeIn', [
    transition('isRight <=> group', [
        query(':enter, :leave', [
            style({
                position: 'absolute',
                left: 0,
                width: '100%',
                height: '100%',
                opacity: 0,
                transform: 'translateY(70%)'
            }),
        ], { optional: true }),
        query(':enter', [
            animate('400ms ease',
                style({ opacity: 1, transform: 'scale(1) translateY(0)' })
            ),
        ], { optional: true })
    ]),
])

export const slider = trigger('routeAnimations', [
    transition('* => isLeft', slideTo('left')),
    transition('isLeft => *', slideTo('right'))
])

function slideTo(direction) {
    const optional = { optional: true }
    return [
        query(':enter, :leave', [
            style({
                position: 'absolute',
                top: 0,
                [direction]: 0,
                width: '100%'
            })
        ], optional),
        query(':enter', [
            style({ [direction]: '-100%' })
        ]),
        group([
            query(':leave', [
                animate('600ms ease', style({ [direction]: '100%' }))
            ], optional),
            query(':enter', [
                animate('600ms ease', style({ [direction]: '0%' }))
            ])
        ])
    ]
}