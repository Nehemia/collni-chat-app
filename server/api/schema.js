const { GraphQLObjectType,
    GraphQLSchema,
    GraphQLString,
    GraphQLNonNull,
    GraphQLList,          // Object deconstrustion
    GraphQLID
} = require('graphql')

const bcrypt = require('bcryptjs')
const User = require('../models/user.model')
const chat = require('../models/chat.model')
const message=require('../models/message.model')




const userType = new GraphQLObjectType({
    name: 'userType',
    fields: () => ({
        username: { type: GraphQLString },

    })
})

const MessageType=new GraphQLObjectType({
    name:'MessageType',
    fields:()=>({
        message:{GraphQLString},
        to:{GraphQLString},
        from:{GraphQLString}

    })
})

// payload that contains data that is going to be send to user on login 
const loginPayload = new GraphQLObjectType({
    name: "loginPayload",
    fields: () => ({
        token: { type: GraphQLString },
        error: { type: GraphQLString }
    })
})


const rootQuery = new GraphQLObjectType({
    name: 'RootQueryType',
    fields: () => ({
        login: {
            type: loginPayload,
            args: {
                username: { type: GraphQLString },
                password: { type: GraphQLString }
            },

            resolve(parent, args, { auth }) {      //handling the operations
                console.log(args)

                return User.findOne({ username: args.username }, { password: 1, _id: 0 })
                    .then((doc) => {
                        console.log('trying loging in', doc)
                        //checking to see if the there is a returned document
                        if (!doc) return false
                        //comparing the returned hashed password with the one provided
                        return bcrypt.compare(args.password, doc.password)

                    })
                    .then((result) => {
                        console.log('result ',result)
                        //checking to see if the result is true
                        if (!result) { return { token: null, error: 'invalid credentials' } }
                        //what happens if the password password is true
                        else {
                            const token = auth.setToken(args.username)
                            return { token, error: null }
                        }

                    })
                    .catch(err => {
                        return err
                        console.log(err)
                    })
            }
        },

        //discoverying the user
        discovery: {
            type: new GraphQLList(userType),
            args: {
                username: { type: GraphQLString }
            },
            resolve(_, args) {
                var name = args.username
                console.log('username')
                return User.find({ username: { $regex: args.username, $options: 'i' } })
                .then(docs=>{
                    return docs

                })

            }
        },
        // //sending mesage
        // replyMessage :{
        //     type:MessageType,
        //     args:{
        //         message:{GraphQLString},
        //         to:{GraphQLString},
        //         from:{GraphQLString}

        //     },
        //     resolve:(_,args) =>{
        //         console.log("mssage",args)
        //         const messag=new message({
        //             message:args.message,
        //             to:args.to,
        //             from:args.from
        //         })
        //         return messag.save()
        //         .then(docs =>{
        //             return docs

        //         })
                
        //     }

        // }
    })
})


const RootMutation = new GraphQLObjectType({
    name: 'RootMutationtype',
    fields: () => ({
        register: {
            type: GraphQLString,
            args: {
                username: { type: GraphQLString },
                password: { type: GraphQLString }
            },

            resolve: (_, args) => {
                console.log('args', args)
                return bcrypt.hash(args.password, 12)
                    .then(hash => {
                        const user = new User({
                            username: args.username,
                            password: hash
                        })
                        return user.save()
                    })
                    .then(doc => {
                        return doc.username


                    })
                    .catch(err => {
                        return err
                    })
            }
        }
    })
})





const schema = new GraphQLSchema({
    query: rootQuery,
    mutation: RootMutation
})

module.exports = schema 