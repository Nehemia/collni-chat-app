var jwt = require('jsonwebtoken');
require('dotenv').config()

 function setToken(username){
    const token = jwt.sign(
        { username },
        process.env.JWT_PRIVATE_KEY,
        {expiresIn :'7d'}
    )

return token
}


function checkToken(req){
   console.log(req.cookies['token'],'token')

   if(req.cookies['token']){
       return jwt.verify(
           req.cookies['token'],
           process.env.JWT_PRIVATE_KEY
       )
   }
}


module.exports = {setToken,checkToken}