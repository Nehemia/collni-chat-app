const {Schema ,model} = require('mongoose')


const userchema = new Schema({
    username : {type :String , required : true ,unique :true} ,
    password : {type : String , required : true , unique: true}  ,
    contacts:[String],
    request:[String],

    sentRequest:[{
		username: {type: String, default: ''}
	}],
	request: [{
		userId: {type: Schema.Types.ObjectId, ref: 'User'},
		username: {type: String, default: ''}
	}],
	friendsList: [{
		friendId: {type: Schema.Types.ObjectId, ref: 'User'},
		friendName: {type: String, default: ''}
	}],
    //  chats : [{type: Schema.Types.ObjectId ,ref : 'Chat'
    //  }]
})

module.exports  = model('User',userchema)