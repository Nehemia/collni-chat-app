const {schema ,model}  = require('mongoose')


const groupSchema = new schema({
  name :{type :String ,require :true ,unique :true},
  members :{type : Array , items :{type:schema.Types.String , ref : 'User'}},
  
  chats : {type : Array ,items:{type:schema.Types.ObjectId ,ref : 'Chat'}}
}
)


module.exports = new model('Group',groupSchema)