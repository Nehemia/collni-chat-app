const { Schema, model } = require('mongoose')


const chatSchema = new Schema({
  username : {type :String , required:true ,unique :true},
  message: {type: String},
  sentFrom: { type: String },
  sendTo: {type: String }
})


module.exports = model('Chat', chatSchema)


// _id :{type :Schema.Types.ObjectId},
    // message : String,
    // replyTo :  {type : Schema.Types.ObjectId },
    // SeenBy : {type : Array ,items:{type:Schema.Types.String ,ref : 'User'}}
  