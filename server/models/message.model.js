const { Schema, model, Model } = require('mongoose')


const messageSchema = new Schema({
    from: { type: String },
    to: { type: String },
    replyTo: { type: String },
    message: String,
    viewed: Boolean
})

const schema = new model('Message', messageSchema)
module.exports = schema