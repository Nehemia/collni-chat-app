const MessageSchema = require('./models/message.model')
const UserSchema=require('./models/user.model')




module.exports = {

    authenticate: (socket, token, callback) => {
        if (!token) {
            return callback(new Error('not authenticated'))
        }
        //get credentials sent by the client
        try {
            var decoded = auth.checkToken(token)
        } catch (e) {
            //inform the callback of auth success/failure
            return callback(new Error("User not found"));
        }
        return callback(null, true);
    },


    postAuthenticate: (socket, data) => {
        var decoded = auth.checkToken(data)
        var username = decoded.username;
        socket.client.user = username;

        const order = (msgArr) => {
            var arr = {}
            var currentList = []
            for (var i = 0; i < msgArr.length; i++) {
                var temp = []
                var current
                if (msgArr[i].from != socket.id) {
                    current = msgArr[i].from
                } else {
                    current = msgArr[i].to
                }



                //checks if user's messages already taken
                if (!arr[current]) {
                    currentList.push(current)
                    for (var j = 0; j < msgArr.length; j++) {
                        if (msgArr[j].from == current || msgArr[j].to == current) {
                            temp.push({
                                from: msgArr[j].from,
                                to: msgArr[j].to,
                                message: msgArr[j].message,
                                time: msgArr[j].time,
                                viewed: msgArr.viewed
                            })
                        }
                    }
                    arr[current] = temp
                }
            }

            var out = []
            for (var i = 0; i < currentList.length; i++) {
                out.push({ id: currentList[i], messages: arr[currentList[i]] })
            }
            return out
        }



        var userId = socket.client.user

        var m = messages.filter((msg) => { return msg.to == userId || msg.from == userId })
        var n = order(m, userId)
        socket.join(userId)
        socket.emit('personalMessages', n)

    },


    //code for the socket part
    socketConfig: (socket) => {
        const filterRoomMessages = (chat) => {
            return messages.filter((msg) => {
                return (
                    (msg.from == socket.id && msg.to == chat) ||
                    (msg.from == chat && msg.to == socket.id))
            })
        }



        socket.on("editMessage", receiver => {
            socket.to(receiver).emit("typing", "typing...")
        })
        socket.on('stopEditMessage', receiver => {
            socket.to(receiver).emit("typing", "")
        })



        socket.on('friendRequest', (data) => {
            //joining
            console.log(data + 'request send from sender to receiver ');

            User.update(
                // { "username": data.to },
                // { $set: { "request": data.from } },

                { "sentRequest.username": data.to },
                { "username": data.from },

                {
                    $push: {
                        sentRequest: {
                            username: req.user.username
                        }
                    }
                },


            )
            // var r={
            //     to:this.global.currentUser,
            //     from:receiver.username,
            //     accept:true,
            //     time:new Date().getTime()
            //   }

            socket.to(data.to).emit('forwading to the receiver', data);

            console.log("silva",data)
        })





        socket.on('reques-accepted', (data) => {
            console.log(data + "response from receiver to sender");
            User.update(
                // { "username": data.to },
                // { $set: { "contact": data.from } },
                { "friendsList.friendName": data.from },
                { "request": data.from },

                {
                    $push: {
                        request: {
                            username: req.user.username
                        }
                    }
                },



            )

            socket.to(data.to).emit('forwading to the sender', data);
        })

        /**
         * handles message transmission between socket rooms
         */
        socket.on('sendMessage', (msg) => {

            //create message object to send to database server
            var message = new MessageSchema({
                from: socket.id,
                to: msg.to,
                message: msg.message,
                time: msg.time,
                viewed: msg.viewed
            })

            //save document to database
            message.save().then(result => {
                console.log(result)
            })

            //only send to message recipient
            socket.to(msg.to).emit('newMessage', msg)

        })

        socket.on('disconnect', () => {
            console.log(socket.client.username, ' disconnected')
        })
    }
}
