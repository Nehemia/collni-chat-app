require('dotenv').config()
const app = require('express')()
const http = require('http').Server(app)
const graphql = require('express-graphql')
const schema = require('./api/schema')
const mongoose = require('mongoose')
const cors = require('cors')
const io = require('socket.io')(http)
const auth = require('./api/auth')
const cookieParser = require('cookie-parser')
const socketModule = require('./socket')





require('socketio-auth')(io, {
    authenticate: socketModule.authenticate,
    postAuthenticate: socketModule.postAuthenticate
})





const dbConn = mongoose.connect(
    process.env.DB_URL, 
    { 
     useNewUrlParser: true, 
     useUnifiedTopology: true 
    })


    
app.use(cookieParser())
app.use(cors({ origin: ['http://localhost:4200'] }))


app.use('/api', graphql((req, res)=>({
    schema,
    graphiql: true,
    context: { req, res, auth }
})))


//setup socket.io
io.on('connection', socket => {
    socketModule.socketConfig(socket)
})






const PORT = 4201
http.listen(PORT, () => {
    console.log('listesting on port ', PORT)
})
