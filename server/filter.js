
const  messageModel = require('./models/message.model')

const findUnread = (username)=> {
    return messageModel.aggregate([
    {

    // selecting the messages that are not yet read
        $match :{
            $and : [
                {to :username},
                {viewed : false}
            ]
        
        }},
        {
    //grouping the messages using the id 
        $group :{
             _id = '$from',
              unread :  {$sum :1},
              messages : {message : '$message'}
            }
        }
]

    )
    }

const findAllMessage = (username)=>{
    return messageModel.aggregate([
        {
            $match :{
             $or:[
                {to:username},
                {from:username}

           ] 
         }
        },

        {
            $group :{
                _id = '$to',
                messages: {$push :{message:'$message',from :'$from'}}
            }
        }
               
    ])
}

`module.exports ={findUnread,indAllMessage}`


